package;

import flixel.util.*;
import flixel.*;
import flixel.math.*;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

class Player extends FlxSprite
{
  public var speed:Float = .01;
  private var _cannon:Cannon;
  public function new(cannon:Cannon)
  {
    _cannon = cannon;
    super();
    makeGraphic(16, 16, FlxColor.BLUE);
    drag.x = drag.y = 10;
    angularDrag = 100;
  }
  override public function update(elapsed:Float):Void
  {
    movement();
    super.update(elapsed);
  }
  private function movement():Void
  {
	if (FlxG.mouse.pressed) {
// relative coords
var relX = FlxG.mouse.x - x;
var relY = FlxG.mouse.y - y;
var angle = Math.atan2(relY, relX);
var opposit = angle + Math.PI;

// shoot
var c = Bullet;
//var c = Bazooka;

var bullet = _cannon.shoot(c, x+(width/2), y+(height/2), angle, velocity.x, velocity.y);

// rebound
var energy = bullet.initialSpeed * bullet.mass * speed;
velocity.set(
	velocity.x + energy * Math.cos(opposit), 
	velocity.y + energy * Math.sin(opposit)
);
angularVelocity += Math.random() * 10;

	}
  }
}
