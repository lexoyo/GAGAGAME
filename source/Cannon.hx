package;

import flixel.util.*;
import flixel.*;
import flixel.group.*;

class Cannon extends FlxGroup
{
  public function new(MaxSize:Int = 100)
  {
    super(MaxSize);
  }
  public function shoot(ClassName:Class<Bullet>, x:Float, y:Float, angle:Float, initialSpeedX:Float, initialSpeedY:Float):Bullet
  {
trace('aaaa', ClassName);
	var bullet = Type.createInstance(ClassName, [
		x + (Math.random() - .5) * 5, 
		y + (Math.random() - .5) * 5
	]);
//	bullet.angle = angle;
	angle += (Math.random() - .5) * (Math.PI/12);
	var speedX = bullet.initialSpeed * Math.cos(angle);
	var speedY = bullet.initialSpeed * Math.sin(angle);
	bullet.velocity.set(
		speedX + initialSpeedX, 
		speedY + initialSpeedY);
	add(bullet);
return bullet;
  }
	override public function update(elapsed:Float):Void {
		super.update(elapsed);
  		forEachDead(function(bullet) {
			trace('remove bullet');
			remove(bullet);
		});
		
	}
}
