package;

import flixel.util.*;
import flixel.*;
import flixel.math.*;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;

class Bullet extends FlxSprite
{
  private var _life = 100;
  public var initialSpeed:Float = 100;
  public function new(X:Float = 0, Y:Float = 0, w:Int = 1, h:Int= 1)
  {
    super(X, Y);
    makeGraphic(w, h, FlxColor.RED);
    drag.x = drag.y = 0;
    mass = w + h;
  }
  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
	if(--_life <= 0) {
		trace('dead', this);
		kill();
	}
  }
}

