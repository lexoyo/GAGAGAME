package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.FlxG;
import flixel.FlxCamera;


class PlayState extends FlxState
{
  private var _player: Player;
  private var _cannon: Cannon;
	override public function create():Void
	{
		super.create();
    _cannon = new Cannon();
    add(_cannon);
    _player = new Player(_cannon);
    _player.screenCenter();
    add(_player);
    FlxG.camera.follow(_player, FlxCameraFollowStyle.SCREEN_BY_SCREEN);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		//FlxG.collide(_player, _cannon, onCollide);
	}
	function onCollide(a, b) {
		trace('onCollide', a, b);
	}
}
