package;

import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.*;
import flixel.FlxG;

class MenuState extends FlxState
{
  override public function create():Void
  {
    trace('create');
    super.create();
    var _btnPlay = new FlxButton(0, 0, "Play", clickPlay);
     _btnPlay.screenCenter();
    add(_btnPlay);
  }

  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
  }

  private function clickPlay():Void {
    trace('click');
    FlxG.switchState(new PlayState());
  }
}
